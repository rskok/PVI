<?php
use Dotenv\Dotenv;
require_once __DIR__ . '/../vendor/autoload.php';

class Database
{
  private $host;
  private $user;
  private $password;
  private $db_name;
  private $connection;

  public function __construct()
  {
    $dotenv = Dotenv::createImmutable(dirname(__DIR__));
    $dotenv->load();

    $this->host = $_ENV['DB_HOST'];
    $this->db_name = $_ENV['DB_NAME'];
    $this->user = $_ENV['DB_USER'];
    $this->password = $_ENV['DB_PASSWORD'];

    $this->connect();
  }

  function __destruct()
  {
    $this->closeConnection();
  }

  private function connect(): void
  {
    $this->connection = new mysqli($this->host, $this->user, $this->password, $this->db_name);

    if ($this->connection->connect_error) {
      die("Connection failed: " . $this->connection->connect_error);
    }

    error_log("[DB] Connected to DB $this->db_name successfully");
    return;
  }

  public function getAllStudents(): array
  {
    $query = "SELECT * FROM students";
    $result = $this->connection->query($query);

    if ($result->num_rows > 0) {
      while ($row = $result->fetch_assoc()) {
        $users[] = $row;
      }
    }

    return $users;
  }

  public function addStudent(): int
  {
    $data = json_decode(file_get_contents("php://input"), true);

    // Prepare and execute the SQL INSERT statement
    $sql = "INSERT INTO students (first_name, last_name, group_name, gender, birthday, status) VALUES ('" .
      $data['firstName'] . "', '" .
      $data['lastName'] . "', '" .
      $data['group'] . "', '" .
      $data['gender'] . "', '" .
      $data['birthday'] . "', '" .
      $data['status'] . "')";

    // Execute the SQL query
    try {
      $this->connection->query($sql);
      error_log("Data inserted successfully.");
      $last_id = $this->connection->insert_id;
      error_log("Last inserted ID is: " . $last_id);
      return $last_id;
    } catch (PDOException $e) {
      error_log("Error: " . $e->getMessage());
    }
  }

  public function updateStudent(): int
  {
    $data = json_decode(file_get_contents("php://input"), true);

    // Prepare and execute the SQL UPDATE statement
    $sql = "UPDATE students SET 
            first_name = '" . $data['firstName'] . "', 
            last_name = '" . $data['lastName'] . "', 
            group_name = '" . $data['group'] . "', 
            gender = '" . $data['gender'] . "', 
            birthday = '" . $data['birthday'] . "', 
            status = '" . $data['status'] . "'
          WHERE id = " . $data['id'];

    // Execute the SQL query
    try {
      $this->connection->query($sql);
      error_log("Data updated successfully.");
      return $data['id'];
    } catch (PDOException $e) {
      error_log("Error: " . $e->getMessage());
    }
  }

  public function deleteStudent(): int
  {
    $data = json_decode(file_get_contents("php://input"), true);

    // Prepare and execute the SQL DELETE statement
    $sql = "DELETE FROM students WHERE id = " . $data['id'];

    // Execute the SQL query
    try {
      $this->connection->query($sql);
      error_log("Data deleted successfully.");
      return $data['id'];
    } catch (PDOException $e) {
      error_log("Error: " . $e->getMessage());
    }
  }

  public function closeConnection(): void
  {
    $this->connection->close();
    error_log("[DB] Connection closed\n");
  }
}
