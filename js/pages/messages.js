const baseUrl = "http://localhost:3000/";

axios.defaults.baseURL = baseUrl;

Vue.createApp({
  data() {
    return {
      showLoginPopup: false,
      showCreateRoomPopup: false,
      showInviteUserPopup: false,
      showAddFriendPopup: false,

      rooms: [],
      messages: [],
      currentRoom: null,
      user: null,

      loginUsername: "",
      roomTitle: "",
      friendsUsername: "",
      newMemberUsername: "",
      message: "",

      loginModal: null,

      socket: null,
    };
  },
  computed: {
    currentUser() {
      return localStorage.getItem("user")
        ? localStorage.getItem("user")
        : this.user?.username;
    },
  },
  methods: {
    async setRooms() {
      try {
        const rooms = await this.fetchRooms();
        this.rooms = rooms;
      } catch (e) {
        this.showErrorMessage(e);
      }
    },
    async fetchRooms() {
      return new Promise((resolve, reject) => {
        if (
          localStorage.getItem("user")?.trim() === "" ||
          localStorage.getItem("user") === null
        ) {
          reject({ message: "Session timed out. Please log in" });
        } else {
          axios
            .get("rooms/" + localStorage.getItem("user"))
            .then((res) => {
              resolve(res.data);
            })
            .catch((e) => {
              const errMsg = e?.response?.data?.message;
              reject({ message: errMsg ? errMsg : e?.message });
            });
        }
      });
    },
    showErrorMessage(axiosError) {
      const errMsg = axiosError?.response?.data?.message;
      generateErrorToast(errMsg ? errMsg : axiosError?.message);
    },
    async createRoom() {
      const room = {
        title: this.roomTitle,
        username: this.currentUser,
        isDirect: false,
      };
      try {
        const createdRoom = await axios.post("rooms", room);
        this.rooms.push(createdRoom.data);
      } catch (e) {
        this.showErrorMessage(e);
      }
    },
    changeRoom(room) {
      if (!localStorage.getItem("user")) {
        return;
      }
      if (this.socket?.connected) {
        this.leaveRoom();
      }
      this.currentRoom = room;
      this.socket = io("http://localhost:3000", {
        extraHeaders: {
          room: room?._id,
        },
      });
      this.socket.once("load_messages", (data) => {
        this.messages = [];
        this.messages = data;
        console.log(data);
      });
      this.socket.on("new_message", (data) => {
        this.messages.push(data);
        return;
      });
    },
    sendMessage() {
      if (this.message?.trim() === "" && !this.currentUser) {
        return;
      }
      this.socket.emit("send_message", {
        username: this.currentUser,
        room_id: this.currentRoom._id,
        content: this.message,
      });
      this.currentMessage = "";
    },
    async login() {
      if (this.loginUsername?.trim() === "") {
        return;
      }
      try {
        const { data: user } = await axios.get("auth/" + this.loginUsername);
        localStorage.setItem("user", user?.username);
        this.user = user;
        this.loginModal.hide();
        this.currentRoom = null;
        this.setRooms();
      } catch (e) {
        this.showErrorMessage(e);
      }
    },
    async inviteUser() {
      if (!this.currentRoom || !this.currentUser) {
        return;
      }
      try {
        const { data: newRoom } = await axios.put("rooms", {
          username: this.newMemberUsername,
          room_id: this.currentRoom?._id,
        });
        this.currentRoom = newRoom;
      } catch (e) {
        this.showErrorMessage(e);
      }
    },
    leaveRoom() {
      this.socket?.disconnect();
      this.messages = [];
      console.log("disconnected");
    },
  },

  mounted() {
    console.log(this.currentUser);
    this.loginModal = new bootstrap.Modal(this.$refs.loginModal, {});
    if (!!!!!this.currentUser || this.currentUser === null || this.currentUser?.trim() === "") {
      this.loginModal.show();
    } else {
      this.setRooms();
    }
  },
}).mount("#app");

// toasts
export default function generateErrorToast(message) {
  // Create the toast container element
  const toastContainer = document.createElement("div");
  toastContainer.classList.add("toast");
  toastContainer.classList.add("toast-lg");
  toastContainer.classList.add("bg-danger");
  toastContainer.setAttribute("role", "alert");
  toastContainer.setAttribute("aria-live", "assertive");
  toastContainer.setAttribute("aria-atomic", "true");
  toastContainer.style =
    "position: absolute; top: 5%; right: 50%; z-index: 100000; width: 300px; height: 100px;";

  // Create the toast header element
  const toastHeader = document.createElement("div");
  toastHeader.classList.add("toast-header");

  const toastHeaderTitle = document.createElement("strong");
  toastHeaderTitle.classList.add("mr-auto");
  toastHeaderTitle.textContent = "Error";
  toastHeader.appendChild(toastHeaderTitle);

  const toastHeaderCloseBtn = document.createElement("button");
  toastHeaderCloseBtn.classList.add("btn-close");
  toastHeaderCloseBtn.setAttribute("type", "button");
  toastHeaderCloseBtn.setAttribute("data-bs-dismiss", "toast");
  toastHeaderCloseBtn.setAttribute("aria-label", "Close");
  toastHeader.appendChild(toastHeaderCloseBtn);

  toastContainer.appendChild(toastHeader);

  // Create the toast body element
  const toastBody = document.createElement("div");
  toastBody.classList.add("toast-body");
  toastBody.textContent = message;

  // Append the toast body to the toast container
  toastContainer.appendChild(toastBody);

  // Append the toast container to the document body
  document.body.appendChild(toastContainer);

  // Initialize the toast
  const toast = new bootstrap.Toast(toastContainer);

  // Show the toast
  toast.show();
}
