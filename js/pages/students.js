import Student from "../models/Student.js";
import generateErrorToast from "../utils.js";

const addStudentModal = new bootstrap.Modal("#addStudentModal");
const delModal = new bootstrap.Modal("#confirmDelete");

const studentTable = document.getElementById("studentTable");
const studentTableBody = document.getElementById("studentTableBody");
const studentForm = document.forms.addStudentForm;
const studentModal = document.getElementById("addStudentModal");
const modalTitle = document.getElementById("addStudentModalLabel");
const submitBtn = document.getElementById("create-student");
const confirmModal = document.getElementById("confirmDelete");
const headerCheckbox = document.querySelector(
  "#studentTable th:first-child input[type=checkbox]"
);

function readStudentForm() {
  const id = parseInt(studentForm.studentId.value, 10);
  const firstName = studentForm.inputFirstName.value;
  const lastName = studentForm.inputLastName.value;
  const group = studentForm.inputGroup.value;
  const gender = studentForm.inputGender.value;
  const birthday = studentForm.inputBirthday.value;

  return new Student(id, firstName, lastName, group, gender, birthday, false);
}

function parseTrStudent(tr) {
  const tds = tr.getElementsByTagName("td");

  const id = parseInt(tr.getAttribute("student-id"), 10);
  const fullName = tds[2].innerText;
  const firstName = fullName.split(" ")[0];
  const lastName = fullName.split(" ")[1];
  const group = tds[1].innerText;
  let gender = "Other";
  if (tds[3].innerText === "M") {
    gender = "Male";
  } else if (tds[3].innerText === "F") {
    gender = "Female";
  }
  const birthday = tds[4].innerText;
  return new Student(id, firstName, lastName, group, gender, birthday, false);
}

function fillStudentForm(student) {
  studentForm.inputFirstName.value = student.firstName;
  studentForm.inputLastName.value = student.lastName;
  studentForm.inputGroup.value = student.group;
  studentForm.inputGender.value = student.gender;

  const parts = student.birthday.split(".");
  studentForm.inputBirthday.value = `${parts[2]}-${parts[1]}-${parts[0]}`;
}

function onAnyCheckboxSelect() {
  console.log("Checkbox selected");
  if (headerCheckbox.checked) {
    headerCheckbox.checked = false;
  }
}

async function updateStudentFetch(studentJson) {
  const response = await fetch("http://localhost:8000/update-user", {
    method: "put",
    body: studentJson,
  });
  if (!response.ok) {
    generateErrorToast(
      "Error updating student. " + response.status + " " + response.statusText
    );
    return response.json();
  }
  const data = await response.json();
  console.log(data);
  return data;
}

function updateStudent() {
  const student = readStudentForm();
  updateStudentFetch(student.generateJson());
  const studentRow = student.generateTrElement();
  studentRow.querySelector("input[type=checkbox]").onclick = () =>
    onAnyCheckboxSelect();
  const tr = document.getElementsByClassName("currently-editing")[0];
  tr.replaceWith(studentRow);

  tr.classList.remove("currently-editing");
  // console.debug(student.generateJson());
}

async function addStudentFetch(studentJson) {
  const response = await fetch("http://localhost:8000/add-user", {
    method: "post",
    body: studentJson,
  });
  const data = await response.json();
  console.log(data);
  return data;
}

async function addStudent() {
  const student = readStudentForm();

  const data = await addStudentFetch(student.generateJson());

  console.log(data);
  student.setId(data.id);
  const studentRow = student.generateTrElement();
  studentRow.querySelector("input[type=checkbox]").onclick = () =>
    onAnyCheckboxSelect();
  studentTableBody.appendChild(studentRow);
  console.debug(student.generateJson());
}

async function validateStudentForm(raction, rmethod, student) {
  return fetch(raction, {
    method: rmethod,
    body: student.generateJson(),
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      return data;
    })
    .catch((error) => {
      console.error(error);
    });
}

// Submitting student form. validation and add/edit student
async function onStudentFormSubmit(event) {
  event.preventDefault();
  // const isValid = studentForm.checkValidity();
  // if (!isValid) {
  //   event.preventDefault();
  //   event.stopPropagation();
  // }
  // studentForm.classList.add("was-validated");

  // if (!isValid) {
  //   return;
  // }

  const student = readStudentForm();

  const response = await validateStudentForm(this.action, this.method, student);
  const success = response.success;

  if (!success) {
    const toast = new bootstrap.Toast(document.getElementById("errorToast"));
    toast.show();
    event.preventDefault();
    return;
  }

  if (event.submitter.textContent === "Create") {
    addStudent();
  } else {
    updateStudent();
  }

  addStudentModal.hide();
  studentForm.reset();
  studentForm.studentId.value = "";
  studentForm.classList.remove("was-validated");
  event.preventDefault();
}

// Add birthday ranges to form
function setBirthdayRanges() {
  const currentDate = new Date();

  // Calculate the maximum date (16 years ago)
  const maxDate = new Date(
    currentDate.getFullYear() - 16,
    currentDate.getMonth(),
    currentDate.getDate()
  )
    .toISOString()
    .split("T")[0];

  // Calculate the minimum date (100 years ago)
  const minDate = new Date(
    currentDate.getFullYear() - 100,
    currentDate.getMonth(),
    currentDate.getDate()
  )
    .toISOString()
    .split("T")[0];

  const birthdayInput = document.getElementById("inputBirthday");
  birthdayInput.setAttribute("min", minDate);
  birthdayInput.setAttribute("max", maxDate);
}

async function deleteStudentFetch(idJson) {
  const response = await fetch("http://localhost:8000/delete-user", {
    method: "delete",
    body: idJson,
  });
  if (!response.ok) {
    generateErrorToast(
      "Error deleting student. " + response.status + " " + response.statusText
    );
    return response.json();
  }

  const data = await response.json();
  console.log(data);
  return data;
}

async function handleDeleteAnswer(confirm, event) {
  if (confirm) {
    const tr = event.relatedTarget.closest("tr");
    studentTableBody.removeChild(tr);
    const id = tr.getAttribute("student-id");
    const idJson = JSON.stringify({ id: parseInt(id, 10) });
    await deleteStudentFetch(idJson);
  }
  delModal.hide();
}

function onConfirmModalOpen(event) {
  const button = event.relatedTarget;
  const student = button.getAttribute("data-bs-student");

  const studentToDel = document.getElementById("studentToDelName");
  studentToDel.innerHTML = student;

  const confirmBtn = document.getElementById("confirmDeleteBtn");
  const cancelBtn = document.getElementById("cancelDeleteBtn");

  confirmBtn.onclick = () => handleDeleteAnswer(true, event);
  cancelBtn.onclick = () => handleDeleteAnswer(false, event);
}

function generateStudentId() {
  if (studentTable.rows.length === 0) {
    return 0;
  }
  const lastTr = studentTable.rows[studentTable.rows.length - 1];
  return parseInt(lastTr.getAttribute("student-id"), 10) + 1;
}

function onStudentModalShow(event) {
  const button = event.relatedTarget;
  const studentIdInput = document.getElementById("studentId");

  if (button.id !== "edit-student") {
    modalTitle.innerText = "Add Student";
    submitBtn.innerText = "Create";
    studentIdInput.value = "-1";
    return;
  }

  modalTitle.innerText = "Edit Student";
  submitBtn.innerText = "Save";

  const student = parseTrStudent(button.closest("tr"));
  studentIdInput.value = student.id;

  fillStudentForm(student);
  button.closest("tr").classList.add("currently-editing");
}

function onHeaderCheckboxSelect() {
  const rowCheckboxes = document.querySelectorAll(
    "tr td:first-child input[type=checkbox]"
  );

  rowCheckboxes.forEach((element) => {
    element.checked = headerCheckbox.checked;
  });
}

async function getAllStudentsFetch() {
  return fetch("http://localhost:8000/get-students", {
    method: "get",
  })
    .then((response) => response.json())
    .then((data) => data)
    .catch((error) => {
      console.error(error);
    });
}

async function fillStudentTable() {
  const students = await getAllStudentsFetch();
  students.forEach((studJson) => {
    const student = new Student(
      studJson.id,
      studJson.first_name,
      studJson.last_name,
      studJson.group_name,
      studJson.gender,
      studJson.birthday,
      studJson.status
    );
    const studentRow = student.generateTrElement();
    studentTableBody.appendChild(studentRow);
  });
}

// setting up everything for students page
export default function initStudents() {
  console.debug("Initializing Students page..");

  setBirthdayRanges();
  fillStudentTable();

  studentForm.addEventListener("submit", onStudentFormSubmit);
  confirmModal.addEventListener("show.bs.modal", onConfirmModalOpen);
  studentModal.addEventListener("show.bs.modal", onStudentModalShow);
  headerCheckbox.addEventListener("change", onHeaderCheckboxSelect);
}
