export default class Student {
  constructor(id, firstName, lastName, group, gender, birthday, status) {
    this.id = id;
    this.firstName = firstName;
    this.lastName = lastName;
    this.group = group;
    this.gender = gender;
    this.birthday = birthday;
    this.status = status;
  }

  setId(id) {
    this.id = id;
  }

  generateTrElement() {
    const tr = document.createElement("tr");
    tr.setAttribute("student-id", this.id);

    let statusIcon = '<i class="fa-solid fa-circle student-status"></i>';

    if (this.status) {
      statusIcon = '<i class="fa-solid fa-circle student-status active"></i>';
    }

    const date = new Date(this.birthday);

    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, "0");
    const day = String(date.getDate()).padStart(2, "0");

    const studentFullname = `${this.firstName} ${this.lastName}`;

    tr.innerHTML = `
      <td><input type="checkbox" /></td>
      <td>${this.group}</td>
      <td>${studentFullname}</td>
      <td>${this.gender.charAt(0)}</td>
      <td>${day}.${month}.${year}</td>
      <td>${statusIcon}</td>
      <td>
        <i id="edit-student" data-bs-toggle="modal" data-bs-target="#addStudentModal" data-bs-student="${studentFullname}" class="fa-regular fa-pen-to-square table-icon"></i>
        <i id="remove-student" data-bs-toggle="modal" data-bs-target="#confirmDelete" data-bs-student="${studentFullname}" class="fa-sharp fa-solid fa-trash table-icon"></i>
      </td>`;

    return tr;
  }

  generateJson() {
    return `
    {
      "id": "${this.id}",
      "firstName": "${this.firstName}",
      "lastName": "${this.lastName}",
      "group": "${this.group}",
      "gender": "${this.gender}",
      "birthday": "${this.birthday}",
      "status": "${this.status}"
    }`
  }
}
