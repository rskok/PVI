export default function generateErrorToast(message) {
  // Create the toast container element
  const toastContainer = document.createElement("div");
  toastContainer.classList.add("toast");
  toastContainer.classList.add("toast-lg");
  toastContainer.classList.add("bg-danger");
  toastContainer.setAttribute("role", "alert");
  toastContainer.setAttribute("aria-live", "assertive");
  toastContainer.setAttribute("aria-atomic", "true");
  toastContainer.style =
    "position: absolute; top: 5%; right: 50%; z-index: 100000; width: 300px; height: 100px;";

  // Create the toast header element
  const toastHeader = document.createElement("div");
  toastHeader.classList.add("toast-header");

  const toastHeaderTitle = document.createElement("strong");
  toastHeaderTitle.classList.add("mr-auto");
  toastHeaderTitle.textContent = "Error";
  toastHeader.appendChild(toastHeaderTitle);
  
  const toastHeaderCloseBtn = document.createElement("button");
  toastHeaderCloseBtn.classList.add("btn-close");
  toastHeaderCloseBtn.setAttribute("type", "button");
  toastHeaderCloseBtn.setAttribute("data-bs-dismiss", "toast");
  toastHeaderCloseBtn.setAttribute("aria-label", "Close");
  toastHeader.appendChild(toastHeaderCloseBtn);

  toastContainer.appendChild(toastHeader);

  // Create the toast body element
  const toastBody = document.createElement("div");
  toastBody.classList.add("toast-body");
  toastBody.textContent = message;

  // Append the toast body to the toast container
  toastContainer.appendChild(toastBody);

  // Append the toast container to the document body
  document.body.appendChild(toastContainer);

  // Initialize the toast
  const toast = new bootstrap.Toast(toastContainer);

  // Show the toast
  toast.show();
}

