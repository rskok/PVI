module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: ["airbnb-base", "prettier"],
  overrides: [],
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
  },
  rules: {
    "jsx-quotes": [2, "prefer-single"],
    "no-param-reassign": [2, { "props": false }],
    'no-console': 'off',
  },
};
