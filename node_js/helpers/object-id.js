const mongoose = require("mongoose");

module.exports = function (id) {
  return new mongoose.Types.ObjectId(id);
};
