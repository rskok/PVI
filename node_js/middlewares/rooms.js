const validator = require("../helpers/validator");
const createRoomValidation = async (req, res, next) => {
  const validationRule = {
    title: "required|string",
    username: "required|string",
    isDirect: "required|boolean",
  };

  await validator(req.body, validationRule, {}, (err, status) =>
    validate(err, status, res, next)
  ).catch((err) => console.log(err));
};

const updateRoomValidation = async (req, res, next) => {
  const validationRule = {
    username: "required|string",
    room_id: "required|string",
  };

  await validator(req.body, validationRule, {}, (err, status) =>
    validate(err, status, res, next)
  ).catch((err) => console.log(err));
};

async function validate(err, status, res, next) {
  if (!status) {
    res.status(400).send({
      success: false,
      status: 400,
      message: "Bad request",
      code: "ERR_BAD_REQUEST",
      data: err,
    });
  } else {
    next();
  }
}
module.exports = {
  createRoomValidation,
  updateRoomValidation,
};
