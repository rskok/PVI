const mongoose = require("mongoose");

const schema = mongoose.Schema({
  title: String,
  users: Array,
  isDirect: Boolean,
});

module.exports = mongoose.model("Room", schema);
