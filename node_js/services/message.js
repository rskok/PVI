const { Message } = require("../models");

async function getMessages(room_id) {
  const messages = await Message.find({ roomId: room_id }).sort({
    createdAt: 1,
  });
  return messages;
}

async function createMessage(username, room_id, content) {
  const message = new Message({
    content,
    sender: username,
    roomId: room_id,
    createdAt: Date.now(),
  });
  return message.save();
}

module.exports = {
  getMessages,
  createMessage,
};
