const { messageService } = require("../services");
const io = require("socket.io");

let socketServer = null;

function connect(server) {
  socketServer = io(server, {
    cors: {
      origin: "*",
      methods: ["GET", "POST"],
    },
  });

  socketServer.on("connection", async (client) => {
    const roomId = client.request.headers["room"];
    client.join(roomId);
    const messages = await messageService.getMessages(roomId);
    socketServer.to(roomId).emit("load_messages", messages);
    client.on("send_message", async (data) => {
      const message = await messageService.createMessage(
        data.username,
        data.room_id,
        data.content
      );
      socketServer.to(roomId).emit("new_message", message);
    });
  });
}

function disconnect(callback) {
  if (socketServer) {
    socketServer.close();
    callback();
  }
}

module.exports = {
  connect,
  disconnect,
};
