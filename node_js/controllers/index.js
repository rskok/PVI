const roomController = require("./room");
const userController = require("./user");

module.exports = {
  userController,
  roomController,
};
