<?php
require_once 'php/database.php';

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, DELETE, PUT');
header("Access-Control-Allow-Headers: X-Requested-With");

$request = $_SERVER['REQUEST_URI'];

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  switch ($request) {
    // case '/':
    //   break;
    // case '':
    //   break;
    case '/validate-user':
      validateUser();
      break;
    case '/add-user':
      addUser();
      break;
    default:
      http_response_code(404);
      break;
  }
}
else if ($_SERVER['REQUEST_METHOD'] === 'PUT') {
  switch ($request) {
    case '/update-user':
      updateUser();
      break;
    default:
      http_response_code(404);
      break;
  }
}
else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
  switch ($request) {
    // case '/':
    //   require __DIR__ . '/views/index.php';
    //   break;
    // case '':
    //   require __DIR__ . '/views/index.php';
    //   break;
    case '/get-students':
      getAllStudents();
      break;
    default:
      http_response_code(404);
      break;
  }
}
else if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
  switch ($request) {
    case '/delete-user':
      deleteStudent();
      break;
    default:
      http_response_code(404);
      break;
  }
}
else if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
  header('Access-Control-Allow-Origin: *');
  header('Access-Control-Allow-Methods: GET, POST, DELETE, PUT');
  header("Access-Control-Allow-Headers: X-Requested-With");
}
else {
  http_response_code(405);
}

function validateUser()
{
  $data = json_decode(file_get_contents("php://input"), true);

  // Initialize the response array
  $response = [];

  $requiredFields = [
    // "id",
    "firstName",
    "lastName",
    "group",
    "gender",
    "birthday",
    "status",
  ];

  // Define the regex patterns
  $patterns = [
    // "id" => '/^\d+$/',
    "firstName" => '/^[A-Za-z]+$/',
    "lastName" => '/^[A-Za-z]+$/',
    "group" => '/^([A-Z]{2,3}-)?[A-Z]{2,3}-[1-9][0-9]{1,2}$/',
    "birthday" => '/^\d{4}-\d{2}-\d{2}$/',
    "gender" => '/^(Male|Female|Other)$/',
    "status" => '/^(true|false)$/',
  ];

  $success = true;

  // Check if all required fields are present and correct
  foreach ($requiredFields as $field) {
    if (!isset($data[$field]) || empty($data[$field])) {
      $response[$field] = "Field is missing or empty";
      $success = false;
    } elseif (!preg_match($patterns[$field], $data[$field])) {
      $response[$field] = "Field is incorrect";
      $success = false;
    } else {
      $response[$field] = $data[$field];
    }
  }

  $responseJsonArr = [
    "success" => $success,
    "response" => $response,
  ];
  $responseJson = json_encode($responseJsonArr);

  echo $responseJson;
}

function addUser()
{
  $db = new Database();
  $id = $db->addStudent();

  echo json_encode(["id" => $id]); 
}

function updateUser()
{
  $db = new Database();
  $id = $db->updateStudent();

  echo json_encode(["id" => $id]); 
}


function getAllStudents()
{
  $db = new Database();
  $students = $db->getAllStudents();
  error_log("Got " . count($students) . " students from DB");

  echo json_encode($students);
}

function deleteStudent()
{
  $db = new Database();
  $id = $db->deleteStudent();

  echo json_encode(["id" => $id]); 
}

?>

